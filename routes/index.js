const siteUrl = "https://medium.com/jeager-industrial-internet-of-things";
const request = require("request");
const cheerio = require("cheerio");
var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", async function(req, res, next) {
  let data = [];
  request(siteUrl, (err, response, html) => {
    if (response.statusCode === 200) {
      const $ = cheerio.load(html);
      $("div.u-paddingTop30.u-paddingBottom30.js-trackPostPresentation").each(
        (index, elements) => {
          let title = $(elements)
            .find("h3.u-contentSansBold div.u-fontSize24.u-xs-fontSize18")
            .text();
          let link = $(elements)
            .find("a.u-block")
            .attr("href");
          let description = $(elements)
            .find("div.u-fontSize18.u-xs-fontSize16")
            .text();
          let image = $(elements)
            .find("a.u-block")
            .css("background-image")
            .split(/"/)[1];
          let author = $(elements)
            .find("a.ds-link")
            .text();
          let time = $(elements)
            .find("time")
            .attr("datetime");

          data.push({
            id: index,
            title,
            link,
            description,
            image,
            author,
            created_at: time
          });
        }
      );
    }
    res.setHeader("Content-Type", "application/json");
    res.status(200).json({ status: "success", data });
  });
});

module.exports = router;
