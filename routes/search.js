const request = require("request");
const cheerio = require("cheerio");
var express = require("express");
var router = express.Router();

/* GET search page. */
router.get("/", async function(req, res, next) {
  if (req.query && req.query.q) {
    let data = [];
    request(
      `https://medium.com/search?q=${req.query.q}`,
      (err, response, html) => {
        if (response.statusCode === 200) {
          const $ = cheerio.load(html);
          $("div.js-block").each((index, elements) => {
            let title = $(elements)
              .find("h3")
              .text();
            let link = $(elements)
              .find("div.postArticle-content a")
              .attr("href");
            let description = $(elements)
              .find("p")
              .text();
            let image = $(elements)
              .find(
                "div.postArticle-content a section div.section-content div.section-inner figure div.aspecRatioPlaceholder div.progressiveMedia img.progressiveMedia-image.js-progressiveMedia-image"
              )
              .attr("src");

            console.log(
              $(elements).find(
                "div.postArticle-content a section div.section-conent div.section-inner figure.graf div.aspectRatioPlaceholder div.progressiveMedia img.progressiveMedia-thumbnail.js-progressiveMedia-thumbnail"
              )
            );
            // let author = $(elements)
            //   .find("a.ds-link")
            //   .text();
            // let time = $(elements)
            //   .find("time")
            //   .attr("datetime");

            data.push({
              id: index,
              title,
              link,
              description,
              image
              // author,
              // created_at: time
            });
          });
        }
        res.setHeader("Content-Type", "application/json");
        res.status(200).json({ status: "success", data });
      }
    );
  } else {
    res.status(404).json({ status: "failure", message: "Not found!" });
  }
});

module.exports = router;
